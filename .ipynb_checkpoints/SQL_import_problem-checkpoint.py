import pyodbc as odbc
import pandas as pd  

def csv_2_sql( server, database, driver='MySQL ODBC 8.0 ANSI Driver',
              username="", password="", filename='Dataset.csv',table="dataset"):
    """
    Read a dataset.csv file, create table, and import the csv content into SQL server. Tested using localhost MySQL server.
    Dateset.csv format is stored in following SQL format
                CUST_ID::[int]
                DATE::[date]
                ORDER_QTY:: [int]
                PRICE_USD:: [decimal(6,2)]   
    
    Requirements
    ------------
    pyodbc: pip install pyodbc
    pandas: pip install pandas
    odbc driver:  mysql: https://dev.mysql.com/doc/connector-odbc/en/connector-odbc-installation.html
                  others: https://github.com/mkleehammer/pyodbc/wiki
                  
    Args:
    -----------
        filename::[str] 
            file path of .csv file

        server::[str]
            SQL server name e.g.
               'localhost\sqlexpress' for a named instance
               'myserver,port' to specify an alternate port
         
        db::[str]
            Database name

        driver::[str]
            odbc connector driver name, use odbc.drivers() to check installed drivers, e.g.
                'SQL Server' :SQL Server
                'MySQL ODBC 8.0 ANSI Driver' : MySQL Server
        
        user::[str]
            The authorized user ID 
            
        pw::[str]
            The authorized database password

        table::[str]
            Name of the table to be create in database to store the data
    
    Returns:
        Status::[int]
            1: data successfully written in to database
            0: otherwise
             
    """
    
   
    df=pd.read_csv(filename)  #Import dataset from CSV
    records=df.values.tolist() 
    conn=SQL_connection(driver, server, database, username, password)
    status=0
    
    if not conn: #connection is not successful
        return status
        
    cursor=conn.cursor() #Create cursor connection
    
    if not table_ready(cursor, database, table): #table with same name exist, no action taken
        print('Table "{}" already exist in database "{}". No data imported'.format(table, database))
        cursor.close()
        conn.close()
        return status
    
    if create_table(cursor, records, table): # if successfully created table insert record
        if insert_records(cursor, records, table):
            status=1
        
    cursor.close() #close connection
    conn.close()
    return status
    
    
def SQL_connection(driver, server, database, username, password):
    """
        Create Database connection instance.
        Check https://www.connectionstrings.com/ for connection string of other database
    """
    #make a f string of connection
    conn_string=f'''
        Driver={{{driver}}};
        Server={server};
        Database={database};
        Trust_Conection=yes;
        Uid={username};
        Pwd={password};
        '''
    try:
        conn=odbc.connect(conn_string) #try to connect
    except odbc.DatabaseError as e:
        print('Datebase Error:') #if failed as database error 
        print(str(e.args[1]))
    except odbc.Error as e:
        print('Connection Error:') #if failed as connection eroor
        print(str(e.args[1]))
    else:    
        return conn    


def table_ready(cursor, database, table):    
    """
    Check if table exist in datatable
    
    Return:
        True: table does not exist
        False: table exist or a error has errored
    """
    #make a f string of check table name
    sql_insert=f'''
        SELECT COUNT(*) 
        FROM information_schema.tables
        WHERE table_schema='{database}'
        AND table_name='{table}'
    '''
    try:
        cursor.execute(sql_insert) #sent f string to database
        table_exist=cursor.fetchall()  #get result
        table_ready = not table_exist[0][0] #extract T/F
    except Exception as e:
        print('Check Table Error') # if error when checking table
        print(str(e.args[1]))
        return(0)
    else:
        return(table_ready)
        
        
def create_table(cursor, records, table):        
    """
    Create Table
    """ 
    #make a f string of create table
    sql_insert=f'''
        CREATE TABLE {table}
        (CUST_ID int, DATE date, ORDER_QTY int, PRICE_USD decimal(6,2))
    '''    
    try:
        cursor.execute(sql_insert)  #sent fstring
        cursor.commit()
    except Exception as e:
        cursor.rollback()   #did not succeed, roll back creation of table
        print('Create Table Error')
        print(str(e.args[1]))
    else:
        return 1
    
def insert_records(cursor, records, table):
    """
    Insert_record
    """ 
    #make a f string of insert value
    sql_insert=f'''
        INSERT INTO {table}
        VALUES (?,?,?,?)
    '''    
    try:
        cursor=conn.cursor() #make a cursor
        cursor.executemany(sql_insert, records) #insert all values to table
        cursor.commit()
    except Exception as e:
        cursor.rollback() #roll back if did not successfully finish all records
        print("Insert Records Error")  
        print(str(e.args[1]))
    else:
        print('Successfully imported into Database') #success, print message. 
        return 1


    
    